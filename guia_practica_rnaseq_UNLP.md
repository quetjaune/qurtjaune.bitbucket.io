https://bitbucket.org/quetjaune/qurtjaune.bitbucket.io/src/master/guia_practica_rnaseq_UNLP.md
#Temas a tratar durante la clase:

####*Se usaran 1 o 2 muestras ejemplos de los datos de RNAseq del hongo Lasiodiplodia theobromae [GSE75978](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE75978)

##**1. Ensamble de novo del transcriptoma**
##**2. Evaluacion de la calidad del ensamble**
##**3. Anotacion funcional de los transcriptos ensamblados** 
##**4. Enriquecimiento funcional en grupos de genes co-rregulados**


##PROGRAMAS REQUERIDOS:
###TRINITY 2.4.0
`wget https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.4.0.tar.gz`
####Descomprimir y enviar a /usr/src/:
`sudo tar zxvf /<ESPECIF_DIR>/Trinity-v2.4.0.tar.gz -C /usr/src/`
###BOWTIE2 2.3.0
`wget https://github.com/BenLangmead/bowtie2/releases/download/v2.3.0/bowtie2-2.3.0-linux-x86_64.zip`
####Descomprimir y enviar a /usr/src/:
`sudo unzip bowtie2-2.3.0-linux-x86_64.zip -d /usr/src/`
###INTEGRATIVE GENOME VIEWER (IGV) 2.4.4
`http://software.broadinstitute.org/software/igv/download`
####Descomprimir IGV:
`sudo unzip /home/quetjaune/Downloads/IGV_2.4.4.zip -d /usr/src/`
###SAMTOOLS 1.6
####desde: `http://www.htslib.org/download/`
####Descomprimimos samtools, lo enviamos a `/usr/src` e instalamos:
`sudo tar -jxf <ESPECIF_DIR>/samtools-1.6.tar.bz2 -C /usr/src/`
####Install:
`cd samtools-1.6`
`./configure`
`make`
`make install`
###TRANSDECODER 5.0.2
`wget https://github.com/TransDecoder/TransDecoder/archive/TransDecoder-v5.0.2.tar.gz`
####Descomprimir archivo en carpeta /usr/src/:
`sudo tar zxvf /<ESPECIF_DIR>/TransDecoder-v5.0.2.tar.gz -C /usr/src/`
###TRINOTATE 3.0.2
####Descargar Trinotate desde [acá](https://github.com/Trinotate/Trinotate/releases/tag/Trinotate-v3.0.2)
`wget https://github.com/Trinotate/Trinotate/archive/v3.0.2.tar.gz`
####Es necesario instalar sqlite3: `sudo apt-get install sqlite3`
####*En caso de que no los tenga instalados: Instalar en caso de que falta la biblioteca: `cpan DBI`
####*En caso de que no los tenga instalados: `sudo apt-get install libdbd-sqlite3-perl`
####*Si no esta instalado, instalar: `sudo apt-get install lighttpd`
####Install HTML-template con:cpan
cpan> `install HTML::Template`
#### Son necesarios tambien R, BIOCONDUCTOR y PAQUETE de R: GOSEQ con sus dependencias.







##**1. Ensamble de lecturas para obtener el transcriptoma de referencia, esto lo haremos mediante el uso de Trinity:**
`/usr/src/trinityrnaseq-Trinity-v2.4.0/Trinity --seqType fq --max_memory 4G --left Sample01_1P.gz --right
Sample01_2P.gz --CPU 3`
###Los archivos `Sample01_1P.gz` y `Sample01_2P.gz`, que corresponden a la salida del Trimmomatic (limpieza de secuencias) los podran descargar desde este [link](https://drive.google.com/drive/folders/1ARfLCGjmyomC9JMEm0sVZqH6ChugGIWI?usp=sharing)
###Esto genera un archivo `Trinity.fasta` que contiene todos los transcriptos generados. Puede tardar 1-2 horas(depende de la capacidad de cada computadora). En caso de que no lo puedan correr en clase, podran descargar el archivo `Trinity.fasta` desde la carpeta compartida en [google drive](https://drive.google.com/drive/folders/10vVauft_hI2cUsuJVZ5fIOU5a9p15s1-?usp=sharing)

###Exploremos el archivo `Trinity.fasta` con comandos en unix:
`head /<ESPECIF_DIR>/Trinity.fasta`
####El resultado que observarás:
>TRINITY_DN14164_c0_g1_i1 len=293 path=[271:0-292] [-1, 271, -2]
GTGATTTCAAACTTGGCTGGTTCGTTATCTCCAACCCTCAGCAGCCCATCACCTTGTGGT
GCTTCGCCGATAAGATATTGTTTCCATTCCTGATACCAATATTCGGCTGCCACTTTGTAC
TCCAAGTATCCTACGCGCAAAAACTCGGAATCGCCGGTGTGGCTACGCTGTAAGAGAACA
CCAGAAAAATTGATGGAATGCTTGTACAGGTCAAACGCTACCAACAGGTAATAGAAATTA
GTGGCGGAGGTCTCCCTATCGTCAAGAATTACCGATGCTGACTCCATCTTCGG
>TRINITY_DN14192_c0_g1_i1 len=213 path=[1:0-212] [-1, 1, -2]
GCGAGGTTACGCATGGCTCCGGTCGCCGCCTGTTGGTCTTGAATCTGCGCAGCTGCGTTG
TAGACATGGTCGATGGTGGGAAACTGTGAGACATTGTAGTCAGCACCCTCTGGGAAATGC
TTTATGAAGTCCCTTTTCGCGCTCTCAAACGCATCAGCGACGGCGGCAGAAGATGCCATG  
###Luego para contar el número de secuencias que tenemos en el archivo usamos la funcion grep:
`grep -c ">" /<ESPECIFI_DIR>/Trinity.fasta`
####¿Que resultado obtuviste? ¿alrededor de 22000?






##**2. Evaluación de la calidad del ensamble obtenido (para más detalles clickea [aquí](https://github.com/trinityrnaseq/trinityrnaseq/wiki/Transcriptome-Assembly-Quality-Assessment)).** 
###En el curso sólo haremos mapeo de las secuencias usando bowtie2 para evaluar la calidad del ensamble. Para esto necesitamos:
####1. construir el indice: 
`bowtie2-build Trinity.fasta Idx_Trinity.fasta`
####2. Alineando la muestra 1 (Se puede tardar de 25-50 min, por eso sólo evaluaremos 1 muestra, tener en cuenta que el archivo sam generado tiene 2.7 GB):
`bowtie2 -p 3 -q -x Idx_Trinity.fasta --very-fast --no-discordant --no-unal -1 /<ESPECIF_DIR>/Sample01_1P.gz -2 /<ESPECIF_DIR>/Sample01_2P.gz -S Sample01.sam`

Revisar las estadísticas del alineamiento. El porcentaje de alineadas concordantes al menos 1 vez es de 88.44%. Mayor a 70% es considerado buen ensamble.  

######*OPCIONAL:Repetir lo mismo con la muestra2: 
`/<ESPECIF_DIR>/bowtie2-2.3.0/bowtie2 -p 3 -q -x Idx_Trinity.fasta --very-fast --no-discordant --no-unal -1 /<ESPECIF_DIR>/Sample02.001.1.fastq.gz -2 <ESPECIF_DIR>/Sample02.001.2.fastq.gz -S Sample02.sam` ######
####3. Si lo queremos visualizar usamos IGV ([Integrative Genome Viewer](http://software.broadinstitute.org/software/igv/download), luego de unas ediciones con [samtools](http://www.htslib.org/download/)):
####A. Convertimos sam a bam:
`/<ESPECIF_DIR>/samtools view -S -b -o Sample01.bam Sample01.sam`
####B. Ordenamos:
`/usr/src/samtools-1.6/samtools sort Sample01.bam -o Sample01.coordSorted.bam`
####C. Construimos indice para samtools:
`/<ESPECIF_DIR>/samtools-1.6/samtools index Sample01.coordSorted.bam`  
####indice para Trinity####
`/<ESPECIF_DIR>/samtools-1.6/samtools faidx<ESPECIFICAR_DIRECTORIO>/Trinity.fasta`
####D. Ahora si lo vemos con IGV:
`/usr/src/IGV_2.4.4/igv.sh -g ../trinity_out_dir/Trinity.fasta Sample01.coordSorted.bam`


##**3.  Luego necesitamos realizar la anotación funcional de los transcriptos. **
###Se inferirá esto con base en la homología con proteínas en bases de datos a las cuales ya se les asignó función (Uniprot) usando [Transdecoder](https://github.com/TransDecoder/TransDecoder/wiki)###
###Para realizar la predicción de regiones codificantes en transcriptos
####Paso 1: identificar marcos abiertos de lectura en los transcriptos MÁS LARGOS obtenidos del ensamble, usar:
`/usr/src/TransDecoder-v5.0.2/TransDecoder.LongOrfs -t ../<ESPECIF_DIR>/Trinity.fasta -m 400` 

#####La opción m=400, indica sólo aquellos ORFs de 400 aa. La seleccionamos ahora a fines prácticos para reducir el tiempo y tamaño de los archivos.
#####En la carpeta desde donde se ejecutó el programa se generará el directorio `Trinity.fasta.transdecoder_dir`  

####Explorar la misma mediante:
`ls`
####Resultado de `ls`: 

  base_freqs.dat    
  base_freqs.dat.ok  
  longest_orfs.cds  
  longest_orfs.gff3  
  longest_orfs.pep  
  __longorfs.ok  

####El archivo .cds nos muestra todas los transcriptos que contienen ORFs, revisarlo mediante:
`head longest_orfs.cds`
>TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.1::m.1 type:5prime_partial len:494 TRINITY_DN14189_c0_g1_i1:1-1482(+)
CTGCTGCCGCGCCGCCCTCAAATGATGCCTTCCTTACTTGATTTGTCAAACGAGCTCCTCCATGAAATTCTTCTGGAGGTTGATCCCAGAGACTTGGCCCGCGTGGCCC
AGTGCTGCCGCTCATTACACACTTTCATAAATGACGGTGAACTCCTTTGGAAGGACCTCTATCTCCAGCATTTTGACGATCCACGCCTGCAGCGGCCAGACGTCAAGC
TTTCTT
>TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.2::m.2 type:complete len:135 TRINITY_DN14189_c0_g1_i1:929-1333(+)
ATGCTGGCTCGTTTCTCGAGGAACAGGATCCGTACGGTGTTACTGGAACCTGGGCAAGGATTGTCTGCTTTCTTGACTATACTGATCTGTACCACTTCAACTTTGAAAC
TGA  
###Luego vemos el encabezado del archivo de anotación con extensión .gff3:
`head longest_orfs.gff3`
####Resultado del `head`:
TRINITY_DN14189_c0_g1_i1 transdecoder
gene
1
1877 .
+
.
ID=TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.1;Name=ORF%20type%3A5prime_partial%20len%3A494%20%28%2B%29
TRINITY_DN14189_c0_g1_i1 transdecoder
mRNA 1
1877 .
+
.
ID=TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.1::m.1.exon1;Parent=TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.1::m.1  

###Luego, vemos el encabezado del archivo con la predicción de aminoácidos:
`head longest_orfs.pep`

>TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.1::m.1 type:5prime_partial len:494 gc:universal TRINITY_DN14189_c0_g1_i1:1-1482(+)
LLPRRPQMMPSLLDLSNELLHEILLEVDPRDLARVAQCCRSLHTFINDGELLWKDLYLQHFDDPRLQRPDVKLSWQAELSRMWKLNLLLSSENMEAKKANFPFISSAIDDLI
AVSSSAQPSLTCGFLSRHFDEPSTLNRDAILCGSALFGQAGSEGQQPFDDECDRQMSAKLHVLNGVPMEPSKRGANPVHSYARSRVYDLRRYTDDTLWGPFMDDGSQRVD
WEKLESILIVLSHNINRFCERTYGRFRPIWNVPFTGVSTNSFRSKPPFRFEDNDEDDDSGMSEAMHIITPATSTDAPNFQRVDSLLNNAGSFLEEQDPYGVTGTWARIVCFLDYTDLYHFNFETDPVPDDQPREPISTQEAIRFITLNLRVRRIEPPGPGDGQELPVVHFTGTSRSMHASWDPNANSKIRGTVRQTPEGHVRWTSFSIFHGEERWRSECVQVGGLRS
ARGMLGTWFDKDYSPEGPAGPTAFWKISDSLIKSDQNPPLLPLS*
>TRINITY_DN14189_c0_g1::TRINITY_DN14189_c0_g1_i1::g.2::m.2 type:complete len:135 gc:universal TRINITY_DN14189_c0_g1_i1:929-1333(+)
MLARFSRNRIRTVLLEPGQGLSAFLTILICTTSTLKLIRFRTINHGSRFQHKKPFASSHSISASEESSLPALVMGKSSQLFISQALPVRCMLRGTPTPIRRSEAPCGRPPRVTFDGH
HSQFSTAKSDGAVSVCK*  

###Búsqueda de homología de proteínas de los ORFs determinados de los transcriptos más largos mediante BLAST y PFAM:
`blastp -query /<ESPECIF_DIR_TRANSDECODER>/longest_orfs.pep -db <ESPECIFI_DIR>/uniprot_sprot.fasta -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 4 > blastp.outfmt6` ##NO EJECUTAR EN CLASE

`hmmscan --cpu 4 --domtblout pfam.domtblout /<ESPECIF_DIR>/Pfam-A.hmm <ESPECIF_DIR_OUT_TRANDEC/longest_orfs.pep` ##NO EJECUTAR EN CLASE
####DEBIDO A QUE ESTAS CORRIDAS TARDAN MUCHO TIEMPO NO LAS HAREMOS DURANTE EL CURSO y buscaremos directamente los archivos de salida de [blast](https://drive.google.com/drive/folders/1JvBqlqU01at2HOmEHO1ert6G8ThfHe-S?usp=sharing) y [pfam](https://drive.google.com/drive/folders/13utbMkLY5myaHx9uWFx1ECMWoHHjSOEa?usp=sharing) en DRIVE. Las bases de datos a utilizar se encuentran en los archivos comprimidos `pfam-A.hmm.tar.gz` y `uniprot_sprot.dat.gz` en [DRIVE](https://drive.google.com/drive/folders/1_QqP-2YcK2y_Qt7C6H8q46Yj-Q1OBsDP?usp=sharing)*
###Una vez que se tienen los archivos de homología con las bases de datos de [Uniprot] y [Pfam] comenzamos a cargarlo en el boilerplate de TRINOTATE:

ESTE [LINK](https://drive.google.com/drive/folders/1MaLXaVEvfNCVcG2KpWvzmKaubtt7Hl7r?usp=sharing)

#### A. Iniciamos la carga de archivos en boilerplate (SQlite) 
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite init --gene_trans_map /<ESPECIF_DIR>/Trinity.fasta.gene_trans_map --transcript_fasta /<ESPECIF_DIR>/Trinity.fasta --transdecoder_pep <ESPECIF_DIR>/Trinity.fasta.transdecoder_dir/longest_orfs.pep`
#### B. Cargamos los resultados de blastp:
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite LOAD_swissprot_blastp /ESPECIF_DIR>/blastp.outfmt6`
#### C. Cargamos los resultados de blastx:  
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite LOAD_swissprot_blastx ../blastx.outfmt6`
#### D. Cargamos los resultados de blastx sobre la base de datos de TREMBL:  
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite LOAD_custom_blast --outfmt6 /<ESPECIF_DIR>/blastx_trembl.outfmt6 --prog blastx --dbtype Trembl`
#### E. Cargamos los datos de homología con PFAM: 
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite LOAD_pfam /<ESPECIF_DIR>/TrinotatePFAM.out`
#### F. Construimos el reporte como archivo de tabla: 
`sudo /usr/src/Trinotate-3.0.2/Trinotate /usr/src/Trinotate-3.0.2/admin/Trinotate.sqlite report > trinotate_annotation_report.xls`

###Explorar los resultados de la anotación mediante [TrinotateWeb](https://trinotate.github.io/TrinotateWeb.html)
###Crear directorio y posicionarse en este:
`mkdir trinotateweb`
###Posicionarse en el home de Trinotate y abrir una página html
`sudo ./run_TrinotateWebserver.pl 8080`
###Abrir un explorador de internet y copiar la siguiente dirección (http://localhost:8080/cgi-bin/index.cgi) o hacer click [aquí](http://localhost:8080/cgi-bin/index.cgi) 
**Indicar la ubicación del archivo Trinotate.sqlite en el cual cargamos los resultados de anotación funcional y ya podemos explorar de forma dinámica los resultados obtenidos!!!**  


##**4. Enriquecimiento funcional de genes co-rregulados**

###Mediante un script en R se agruparon los genes de acuerdo al patrón de expresión en cada comparación de interés. Ver [heatmap](https://drive.google.com/drive/folders/1MfXoDtWlnuyOe0MHPT6VYUWGISbZovru?usp=sharing)
###Los archivos requeridos se pueden descargar de la carpeta [clusters_corregulated_genes](https://drive.google.com/drive/folders/1UWVNQz2ihQcr36xmYHbBNFIArf1aRj_O?usp=sharing): 
1. gene_lengths_4_cluster_enrich
2. go_assignments_clust_enrich
3. all_IDs_clust_enrich 
4. las lista de ID de genes en factor_labeling_cluster1.txt y factor_labeling_cluster3.txt 
###Luego ejecutar:
`<ESPECIF_DIR>/trinityrnaseq-Trinity-v2.4.0/Analysis/DifferentialExpression/run_GOseq.pl --factor_labeling factor_labeling_cluster1.txt --GO_assignments go_assignments_clust_enrich --lengths gene_lengths_4_cluster_enrich --background all_IDs_clust_enrich`
###¿Qué resultados obtienen? ¿Cuantas categorías resultan enriquecidas entre ese grupo de genes corregulados? ¿Cuantas resultan empobrecidas?
###Ahora hacerlo para la lista de genes coexpresados en el cluster3 y responder a las mismas preguntas que nos hicimos para el cluster1
###¿A que se deben estas diferencias entre estos grupos? ¿Qué nos dice respecto a la capacidad biológica del organismo para contender contra el estrés? Puede resultar de ayuda consultar la siguiente [gráficas](https://drive.google.com/drive/folders/1_1HkGPDSNYyRSQW6I8xSDnWT0riyGxOX?usp=sharing)


